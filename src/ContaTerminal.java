import java.util.Locale;
import java.util.Scanner;

public class ContaTerminal {
    public static void main(String[] args) {

        // Cria objeto scanner
        Scanner scanner = new Scanner(System.in).useLocale(Locale.US);

        // Inicia as variaveis
        int numero;
        String agencia;
        String nomeCliente;
        double saldo;

        // Mensagem de boas vindas
        System.out.println("Seja bem-vindo ao banco CampoyCash!");

        System.out.println("Entre com o seu nome: ");
        nomeCliente = scanner.nextLine();

        // Solicita dados para cadastro
        System.out.println("Entre com o número da conta: ");
        numero = scanner.nextInt();

        System.out.println("Entre com o número da agencia: ");
        agencia = scanner.next();

        System.out.println("Entre com o seu deposito inicial R$: ");
        saldo = scanner.nextDouble();


        String mensagem = "Olá %s, obrigado por criar uma conta em nosso banco, sua agência é %s, conta %d e seu saldo R$ %.2f já está disponível para saque";
        mensagem = String.format(mensagem, nomeCliente, agencia, numero, saldo);
        System.out.println(mensagem);

    }
}